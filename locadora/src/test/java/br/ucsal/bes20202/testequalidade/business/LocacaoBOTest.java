package br.ucsal.bes20202.testequalidade.business;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20202.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20202.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20202.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes UNIT�?RIOS para os métodos da classe LocacaoBO (utilize o MOCKITO).
 * 
 * @author claudioneiva
 *
 */

public class LocacaoBOTest {
	private static VeiculoDAO veiculoDAO;
	private static LocacaoBO locacaoBO;

	@BeforeAll
	public static void setupAll() {
		veiculoDAO = mock(VeiculoDAO.class);
		locacaoBO = new LocacaoBO(veiculoDAO);
	}
	
	@BeforeEach
	public void setup() {
		locacaoBO = new LocacaoBO(veiculoDAO);
	}
	
	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 * @throws VeiculoNaoEncontradoException 
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		List<String> placas = null;
		List<Veiculo> veiculos = new ArrayList<>();
		LocalDate dataReferencia = LocalDate.now();
		Integer quantidadeDiasLocacao = 3;
		Integer ano = dataReferencia.getYear();
		
		Double expectedResult = (2d*3*100) + (3d*3*90);
		Double actualResult = 0d;
		
		VeiculoBuilder veiculoBuilder =  VeiculoBuilder.umVeiculo();
		
	    Veiculo veiculo1 = veiculoBuilder.mas().umVeiculoDisponivel().comFabricacao(ano -1).comPlaca("AAA1A11").build();
		Veiculo veiculo2 = veiculoBuilder.mas().umVeiculoDisponivel().comFabricacao(ano -1).comPlaca("BBB2B22").build();
		Veiculo veiculo3 = veiculoBuilder.mas().umVeiculoDisponivel().comFabricacao(ano -8).comPlaca("CCC3C33").build();
		Veiculo veiculo4 = veiculoBuilder.mas().umVeiculoManutencao().comFabricacao(ano -8).comPlaca("DDD4D44").build();
		Veiculo veiculo5 = veiculoBuilder.mas().umVeiculoLocado().comFabricacao(ano -8).comPlaca("EEE5E55").build();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		
		when(veiculoDAO.obterPorPlacas(placas)).thenReturn(veiculos);
		
		actualResult = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);
		
		Assertions.assertEquals(expectedResult, actualResult);
	}

}
