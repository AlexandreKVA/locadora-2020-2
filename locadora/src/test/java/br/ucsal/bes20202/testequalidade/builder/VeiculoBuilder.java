package br.ucsal.bes20202.testequalidade.builder;

import br.ucsal.bes20202.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20202.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	
	private static final String PLACA_DEFAULT = "ABC1D23";
	private static final Integer ANO_FABRICACAO_DEFAULT = 2020;
	private static final Modelo MODELO_DEFAULT = new Modelo("Onix");
	private static final Double DIARIA_DEFAULT = 100.0;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = null;
	
	private String placa = PLACA_DEFAULT;
	private Integer anoFabricacao = ANO_FABRICACAO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = DIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;
	
	private VeiculoBuilder() {
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public static VeiculoBuilder umVeiculoDisponivel() {
		return new VeiculoBuilder().comSituacao(SituacaoVeiculoEnum.DISPONIVEL);
	}
	
	public static VeiculoBuilder umVeiculoManutencao() {
		return new VeiculoBuilder().comSituacao(SituacaoVeiculoEnum.MANUTENCAO);
	}
	
	public static VeiculoBuilder umVeiculoLocado() {
		return new VeiculoBuilder().comSituacao(SituacaoVeiculoEnum.LOCADO);
	}
	
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double n) {
		this.valorDiaria = n;
		return this;
	}
	
	public VeiculoBuilder comFabricacao(Integer n) {
		this.anoFabricacao = n;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoBuilder mas() {
		return new VeiculoBuilder()
				.comModelo(modelo)
				.comPlaca(placa)
				.comValorDiaria(valorDiaria)
				.comSituacao(situacao);
	}
	
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		
		return veiculo;
	}
	

}

